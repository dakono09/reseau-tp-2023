# TP4
## I. DHCP Client
🌞  Déterminer
l'adresse du serveur DHCP
```
ipconfig /all
 Serveur DHCP . . . . . . . . . . . . . : 10.33.79.254
```

l'heure exacte à laquelle vous avez obtenu votre bail DHCP
```
 Bail obtenu. . . . . . . . . . . . . . : vendredi 27 octobre 2023 08:58:23
```
l'heure exacte à laquelle il va expirer
```
Bail expirant. . . . . . . . . . . . . : samedi 28 octobre 2023 08:58:25
```

🌞 Capturer un échange DHCP

forcer votre OS à refaire un échange DHCP complet

utiliser Wireshark pour capturer les 4 trames DHCP
```
PS C:\Users\Session Invité> ipconfig /release
[lien pour les trames DHCP](./capture%20des%20trames.pcapng)
```

🌞 Analyser la capture Wireshark

parmi ces 4 trames, laquelle contient les informations proposées au client ? En cliquant sur l'une des 4 trames, et en dépliant la partie DHCP (en bas dans l'interface de Wireshark) vous pourrez repérer ces informations
```
Les trames DHCP OFFER et DHCP ACK contiennent les informations proposées au client. Ces informations sont par exemple : l'adresse ip du client, le type de message, 
```
# II. Serveur DHCP
# 1. Topologie

```

```
# 2. Table d'adressage
```
A l'aide de la méthode dans le memo rocky linux on change attribue à chacun l'adresse demandée

```

# 3. Setup topologie
🌞 Preuve de mise en place

depuis dhcp.tp4.b1, envoyer un ping vers un nom de domaine public (pas une IP)

depuis node2.tp4.b1, envoyer un ping vers un nom de domaine public (pas une IP)

depuis node2.tp4.b1, un traceroute vers une IP publique pour montrer que vos paquets à destination d'internet passent bien par le router.tp4.b1

```

```
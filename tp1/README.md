# tp1


1. Affichage d'informations sur la pile TCP/IP locale
```
PS C:\Users\Session Invité> ipconfig

Configuration IP de Windows


Carte Ethernet Ethernet 2 :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :

Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::ef4c:3b9b:306e:db83%10
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.56.1
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . :

Carte réseau sans fil Connexion au réseau local* 1 :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :

Carte réseau sans fil Connexion au réseau local* 2 :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :

Carte Ethernet VMware Network Adapter VMnet1 :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::97f2:e7e:17c8:f09d%17
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.152.1
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . :

Carte Ethernet VMware Network Adapter VMnet8 :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::5bfe:421b:961f:6b51%7
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.37.1
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . :

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::6c1b:5e74:e80c:2f34%14
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.50.173
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.51.254

```
Affichez votre gateway
```
 Passerelle par défaut.:    10.33.51.254
```
Déterminer la MAC de la passerelle
```
 Adresse physique . . . . : DC-F5-05-FC-36-03
```
Trouvez comment afficher les informations sur une carte IP (change selon l'OS)
```
SSID :	WiFi@YNOV
Protocole :	Wi-Fi 4 (802.11n)
Type de sécurité :	WPA2 - Entreprise
Type d’informations de connexion :	Microsoft: PEAP (Protected EAP)
Bande passante réseau :	2,4 GHz
Canal réseau :	2
Vitesse de connexion (Réception/Transmission) :	72/72 (Mbps)
Adresse IPv6 locale du lien :	fe80::6c1b:5e74:e80c:2f34%14
Adresse IPv4 :	10.33.50.173
Serveurs DNS IPv4 :	10.33.10.2
8.8.8.8
Fabricant :	Qualcomm Atheros Communications Inc.
Description :	Qualcomm Atheros QCA9377 Wireless Network Adapter
Version du pilote :	12.0.0.1159
Adresse physique (MAC) :	DC-F5-05-FC-36-03
```
2. Modifications des informations
Utilisez l'interface graphique de votre OS pour changer d'adresse IP :
```
Adresse IPv4 : 10.33.50.170
Au cours de cette opération, il est possible de perdre son accès à Internet car la nouvelle adresse ip n'est pas compatible avec le wi-fi.
```
III. Manipulations d'autres outils/protocoles côté client
🌞Exploration du DHCP, depuis votre PC
afficher l'adresse IP du serveur DHCP du réseau WiFi YNOV
```
 Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Qualcomm Atheros QCA9377 Wireless Network Adapter
   Adresse physique . . . . . . . . . . . : DC-F5-05-FC-36-03
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::6c1b:5e74:e80c:2f34%14(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.50.173(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : mardi 17 octobre 2023 09:02:46
   Bail expirant. . . . . . . . . . . . . : mercredi 18 octobre 2023 09:02:46
   Passerelle par défaut. . . . . . . . . : 10.33.51.254
   Serveur DHCP . . . . . . . . . . . . . : 10.33.51.254
   IAID DHCPv6 . . . . . . . . . . . : 215807237
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-27-98-BF-7D-DC-F5-05-FC-36-03
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       8.8.8.8
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```

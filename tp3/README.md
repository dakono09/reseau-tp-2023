# TP3

## I. ARP

### 1. Echange ARP

🌞Générer des requêtes ARP

effectuer un ping d'une machine à l'autre
```
[daryl@localhost ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=0.925 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=0.579 ms
64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=0.669 ms
```
observer les tables ARP des deux machines
```
machine john : [daryl@localhost ~]$ ip neigh show
10.3.1.12 dev enp0s3 lladdr 08:00:27:a1:e0:d2 STALE
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:42 DELAY

machine marcel : [daryl@localhost ~]$ ip neigh show
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:42 REACHABLE
10.3.1.11 dev enp0s3 lladdr 08:00:27:d6:70:54 STALE
```
répérer l'adresse MAC de john dans la table ARP de Marcel et vice-versa
```
Adresse MAC de john :  lladdr 08:00:27:d6:70:54
Adresse MAC de marcel :  lladdr 08:00:27:a1:e0:d 
```
prouvez que l'info est correcte (que l'adresse MAC que vous voyez dans la table est bien celle de la machine correspondante)
° une commande pour voir la MAC de marcel dans la table ARP de john : 10.3.1.12 dev enp0s3 lladdr 08:00:27:a1:e0:d2 STALE
° et une commande pour afficher la MAC de marcel, depuis marcel :
```
 ip a et on constate que la MAC est pareille que celle qu'on regarde cgez john 
```
2. Analyse de trames
🌞Analyse de trames

utilisez la commande tcpdump pour réaliser une capture de trame
```
sudo tcpdump not port 22
[sudo] password for daryl:
dropped privs to tcpdump
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on enp0s3, link-type EN10MB (Ethernet), snapshot length 262144 bytes


11:10:49.184515 IP 10.3.1.12 > localhost.localdomain: ICMP echo request, id 1, seq 1, length 64
11:10:49.184617 IP localhost.localdomain > 10.3.1.12: ICMP echo reply, id 1, seq 1, length 64
```
videz vos tables ARP, sur les deux machines , puis effectuez un ping
```
effectuer la commande  sudo ip neigh flush all sur les 2 machines 
ping 10.3.1.11 sur une machine
sudo tcpdump not port 22 -w tp3_arp.pcapng
dropped privs to tcpdump
tcpdump: listening on enp0s3, link-type EN10MB (Ethernet), snapshot length 262144 bytes
13 packets captured
15 packets received by filter
0 packets dropped by kernel
PS C:\Users\Session Invité> scp daryl@10.3.1.11:/home/daryl/tp3_arp.pcapng .\Downloads\
daryl@10.3.1.11's password:
./Downloads//tp3_arp.pcapng: Broken pipe
PS C:\Users\Session Invité> scp daryl@10.3.1.11:/home/daryl/tp3_arp.pcapng .
daryl@10.3.1.11's password:
tp3_arp.pcapng                                   100% 1991     2.0MB/s   00:00
```

```
[lien vers la capture pcap](./bonne_arp.pcap)
```
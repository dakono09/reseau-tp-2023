## TP6 : Un LAN maîtrisé meo ?

# I. Setup routeur
🖥️ Machine router.tp6.b1
ajoutez lui aussi une carte NAT en plus de la carte host-only (privé hôte en français) pour qu'il ait un accès internet

toutes les autres VMs du TP devront utiliser ce routeur pour accéder à internet

n'oubliez pas d'activer le routage vers internet sur cette machine :
```
 sudo firewall-cmd --add-masquerade --permanent
$ sudo firewall-cmd --reload
```
🖥️ Machine john.tp6.b1
testez tout de suite avec john que votre routeur fonctionne et que vous avez un accès internet
```
[daryl@john ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=55 time=26.2 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=55 time=27.3 ms
^C
--- 1.1.1.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 26.190/26.739/27.289/0.549 ms

```
# II. Serveur DNS
 1. Présentation
 
2. Setup
🖥️ Machine dns.tp6.b1
```
[daryl@dns ~]$ sudo dnf install -y bind bind-utils
[sudo] password for daryl:
Last metadata expiration check: 0:11:26 ago on Fri 17 Nov 2023 11:07:09 AM CET.
Package bind-utils-32:9.16.23-11.el9_2.2.x86_64 is already installed.
Dependencies resolved.

```
🌞 Dans le rendu, je veux

un cat des 3 fichiers de conf (fichier principal + deux fichiers de zone)
```
° Fichier Principal :
[daryl@dns ~]$ sudo cat /etc/named.conf
//
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//

options {
        listen-on port 53 { 127.0.0.1; any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { localhost; any; };
        allow-query-cache { localhost; any; };

        /*
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable
           recursion.
         - If your recursive DNS server has a public IP address, you MUST enable access
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface
        */
        recursion yes;

        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";
        geoip-directory "/usr/share/GeoIP";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
        include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "1.4.10.in-addr.arpa" IN {
        type master;
        file "tp6.b1.rev";
        allow-update { none; };
        allow-query { any; };

};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";


Fichier de zone:
[daryl@dns ~]$ sudo cat /var/named/tp6.b1.db
TTL 86400
@ IN SOA dns.tp6.b1. admin.tp6.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.tp6.b1.

; Enregistrements DNS pour faire correspondre des noms à des IPs
dns       IN A 10.6.1.101
john      IN A 10.6.1.11

Fichier de zone inverse :
[daryl@dns ~]$ sudo cat /var/named/tp6.b1.db
TTL 86400
@ IN SOA dns.tp6.b1. admin.tp6.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.tp6.b1.

; Enregistrements DNS pour faire correspondre des noms à des IPs
dns       IN A 10.6.1.101
john      IN A 10.6.1.11
[daryl@dns ~]$ ^C
[daryl@dns ~]$ sudo cat /var/named/tp6.b1.rev
$TTL 86400
@ IN SOA dns.tp6.b1. admin.tp6.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.tp6.b1.

; Reverse lookup
101 IN PTR dns.tp6.b1.
11 IN PTR john.tp6.b1.

```
un systemctl status named qui prouve que le service tourne bien
```
[daryl@dns ~]$ sudo systemctl status named
● named.service - Berkeley Internet Name Domain (DNS)
     Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; >
     Active: active (running) since Wed 2023-11-22 21:20:10 CET; 17mi>
   Main PID: 707 (named)
      Tasks: 5 (limit: 4604)
     Memory: 25.0M
        CPU: 320ms
     CGroup: /system.slice/named.service
             └─707 /usr/sbin/named -u named -c /etc/named.conf

Nov 22 21:20:10 dns.tp6.b1 named[707]: network unreachable resolving >
Nov 22 21:20:10 dns.tp6.b1 named[707]: network unreachable resolving >
Nov 22 21:20:10 dns.tp6.b1 named[707]: zone 1.0.0.0.0.0.0.0.0.0.0.0.0>
Nov 22 21:20:10 dns.tp6.b1 named[707]: resolver priming query complete
Nov 22 21:20:10 dns.tp6.b1 named[707]: zone localhost/IN: loaded seri>
Nov 22 21:20:10 dns.tp6.b1 named[707]: all zones loaded
Nov 22 21:20:10 dns.tp6.b1 named[707]: running
Nov 22 21:20:10 dns.tp6.b1 named[707]: managed-keys-zone: Unable to f>
Nov 22 21:20:10 dns.tp6.b1 systemd[1]: Started Berkeley Internet Name>
Nov 22 21:20:12 dns.tp6.b1 named[707]: listening on IPv4 interface en>
lines 1-20/20 (END)

```

une commande ss qui prouve que le service écoute bien sur un port
```
[daryl@dns ~]$ ss -n -l
tcp         LISTEN        0             10                                                   10.6.1.101:53                             0.0.0.0:*
tcp         LISTEN        0             10                                                    127.0.0.1:53                             0.0.0.0:*
```
🌞 Ouvrez le bon port dans le firewall

grâce à la commande ss vous devrez avoir repéré sur quel port tourne le service `
```
Oui c'est le port 53
```
ouvrez ce port dans le firewall de la machine dns.tp6.b1 (voir le mémo réseau Rocky)
```
[daryl@dns ~]$ sudo firewall-cmd --add-port=53/tcp --permanent
[sudo] password for daryl:
success

```
3. Test
🌞 Sur la machine john.tp6.b1

configurez la machine pour qu'elle utilise votre serveur DNS quand elle a besoin de résoudre des noms

voir mémo pour ça aussi
john ne doit connaître qu'un seul serveur DNS : le vôtre
une seule IP renseignée dans le fichier resolv.conf
assurez-vous que vous pouvez :

résoudre des noms comme john.tp6.b1 et dns.tp6.b1

mais aussi des noms comme www.ynov.com
```

```
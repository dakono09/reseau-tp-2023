# TP2

I. Setup IP
🌞 Mettez en place une configuration réseau fonctionnelle entre les deux machines
les deux IPs choisies, en précisant le masque
```
Adresse PC1 : 10.10.10.1
Adresse PC2 : 10.10.10.2
Masque de sous-réseau : 255.255.255.0
Adresse de réseau : 10.10.10.0
Adresse de diffusion : 10.10.10.4
```
🌞 Prouvez que la connexion est fonctionnelle entre les deux machines
PS C:\Users\Session Invité> ping 10.10.10.2
```
Envoi d’une requête 'Ping'  10.10.10.2 avec 32 octets de données :
Réponse de 10.10.10.1 : octets=32 temps=35 ms TTL=128
Réponse de 10.10.10.1 :  octets=32 temps=35 ms TTL=128
Réponse de 10.10.10.1 :  octets=32 temps=35 ms TTL=128
Réponse de 10.10.10.1 :  octets=32 temps=35 ms TTL=128 

Statistiques Ping pour 10.10.10.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%), Durée approximative des boucles en millisecondes : Minimum = 35 ms, Maximum = 143ms, Moyenne = 62ms
    ```

🌞 Wireshark it
déterminez, grâce à Wireshark, quel type de paquet ICMP est envoyé par ping
```
 ICMP Echo Request car ils sont utilisés pour tester la connectivité du réseau et le temps de réponse pour le ping
 Pour le pong c'est Echo Request
```
🌞 Check the ARP table
utilisez une commande pour afficher votre table ARP
```
 arp -a

Interface : 192.168.37.1 --- 0x7
  Adresse Internet      Adresse physique      Type
  192.168.37.254        00-50-56-f7-f0-ae     dynamique
  192.168.37.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 192.168.56.1 --- 0xa
  Adresse Internet      Adresse physique      Type
  192.168.56.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.10.10.1 --- 0xe
  Adresse Internet      Adresse physique      Type
  10.10.10.2            b4-2e-99-f6-83-4f     dynamique
  10.10.10.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```
déterminez la MAC de votre binome depuis votre table ARP
```
la MAC de mon binome est :  b4-2e-99-f6-83-4f d'après le tableau des arp
 Adresse Internet      Adresse physique      Type
  10.10.10.2            b4-2e-99-f6-83-4f     dynamique
```
déterminez la MAC de la gateway de votre réseau
```
 ff-ff-ff-ff-ff-ff
```
celle de votre réseau physique, WiFi, genre YNOV, car il n'y en a pas dans votre ptit LAN
```
 00-50-56-f7-f0-ae d'après le tableau ci dessus
```
🌞 Manipuler la table ARP
utilisez une commande pour vider votre table ARP
```
arp -a . La suppression de l'entrée ARP a échoué : L'opération demandée nécessite une élévation
```
prouvez que ça fonctionne en l'affichant et en constatant les changements
```
Chez moi ca ne fonctionne pas
```

ré-effectuez des pings, et constatez la ré-apparition des données dans la table ARP
```

```
🌞 Wireshark it

vous savez maintenant comment forcer un échange ARP : il sufit de vider la table ARP et tenter de contacter quelqu'un, l'échange ARP se fait automatiquement
mettez en évidence les deux trames ARP échangées lorsque vous essayez de contacter quelqu'un pour la "première" fois

déterminez, pour les deux trames, les adresses source et destination
déterminez à quoi correspond chacune de ces adresses
```

III. DHCP
🌞 Wireshark it

identifiez les 4 trames DHCP lors d'un échange DHCP
```
Les 4 trames DHCP sont : DHCP Discover ; DHCP Offer ; DHCP Request ; DHCP Acknoxledgment . Ces 4 trames forment un échange DHCP de base qui permet au client de recevoir une adresse IP et d'autres informations réseau.
```

Fin du TP veuillez pardonner l'allure de mon travail car en effet je ne maitrise pas encore bien les notions de README.md .


# TP5 : TCP, UDP et services réseau
# I. First steps
🌞 **Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP**
déterminez, pour chaque application :
IP et port du serveur auquel vous vous connectez
le port local que vous ouvrez pour vous connecter

Discord utilise du TCP et de l'UDP, je me connecte à l'ip : ```66.22.198.53``` ; au port ```50003 ``` et le port local est ``` 59642```
[capture1](tp5_service_1.pcapng)

Telegram utilise du TCP, je me connecte à l'ip : ```204.79.197.200```; au port ```443``` et le port local est ``` 50858```
[capture2](tp5_service_2.pcapng)
Microsoft Teams utilise du TCP, je me connecte à l'ip : ``` 40.126.32.133``` ; au port ````443`` et le port local est  ```50461```
[capture3](tp5_service_3.pcapng)

Microsft Store utilise du TCP , je me connecte à l'ip : ``` 13.107.246.42``` ; au port ``` 443``` et le port local est ```50576```
[capture4](tp5_service_4.pcapng)

Le site YDays d'YNOV utilise du TCP , je me connecte à l'ip : ``` 152.228.156.33``` ; au port ``` 443``` et le port local est ``` 50679```
[capture5](tp5_service_5.pcapng)
🌞 Demandez l'avis à votre OS

utilisez la commande adaptée à votre OS pour repérer,
 dans la liste de toutes les connexions réseau établies, la connexion que vous voyez dans Wireshark,
 
  pour chacune des 5 applications
lancez votre terminal en admin pour avoir toutes les infos

```
PS C:\Windows\system32> netstat -n -a -b
  TCP    10.33.77.102:51030     104.18.48.115:443      ESTABLISHED
 [Discord.exe]
  TCP    10.33.77.102:50825     149.154.167.92:443     ESTABLISHED
 [Telegram.exe]
   TCP    10.33.77.102:51319     104.115.89.237:443     ESTABLISHED
 [WinStore.App.exe]
  TCP    10.33.77.102:51338     10.33.83.226:8009      SYN_SENT
 [chrome.exe]
```


# II. Setup Virtuel
 # 1. SSH
 🌞 Examinez le trafic dans Wireshark

déterminez si SSH utilise TCP ou UDP

pareil réfléchissez-y deux minutes, logique qu'on utilise pas UDP non ? 
```
SSH utilise évidemment du TCP
```
repérez le 3-Way Handshake à l'établissement de la connexion

c'est le SYN SYNACK ACK

repérez du trafic SSH
repérez le FIN ACK à la fin d'une connexion

[capture6](tp5_3_way.pcapng)


# 2. Routage
🌞 Prouvez que


node1.tp5.b1 a un accès internet

node1.tp5.b1 peut résoudre des noms de domaine publics (comme www.ynov.com)

```
Accès à Internet
[daryl@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=116 time=88.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=116 time=91.1 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=116 time=69.5 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=116 time=124 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms

Peut résoudre
[daryl@localhost ~]$ ping www.ynov.com
PING www.ynov.com (172.67.74.226) 56(84) bytes of data.
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=1 ttl=56 time=123 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=2 ttl=56 time=113 ms
^C64 bytes from 172.67.74.226: icmp_seq=3 ttl=56 time=121 ms

--- www.ynov.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 11523ms
```

# 3. Serveur Web
🌞 Installez le paquet nginx

avec une commande dnf install
```
 sudo dnf install nginx après l'installation commence
```

🌞 Créer le site web
`````
[daryl@localhost ~]$ sudo mkdir -p /var/www/site_web_nul/
[sudo] password for daryl:
[daryl@localhost ~]$ cd /var/www/site_web_nul/
[daryl@localhost site_web_nul]$ sudo nano index.html
[daryl@localhost site_web_nul]$
`````
🌞 Donner les bonnes permissions
```
[daryl@localhost /]$ sudo chown -R nginx:nginx /var/www/site_web_nul
```
🌞 Créer un fichier de configuration NGINX pour notre site web
```
[daryl@localhost /]$ sudo touch /etc/nginx/conf.d/site_web_nul.conf
[sudo] password for daryl:
[daryl@localhost /]$ sudo nano touch /etc/nginx/conf.d/site_web_nul.conf
[sudo] password for daryl:


```
🌞 Démarrer le serveur web !
```
[daryl@localhost /]$ sudo systemctl start nginx
[daryl@localhost /]$

```
🌞 Ouvrir le port firewall
```

```

🌞 Visitez le serveur web !
```
[daryl@localhost ~]$ curl http://10.5.1.12
<h1>MEOW</h1>
[daryl@localhost ~]$

```

🌞 Visualiser le port en écoute
```
[daryl@localhost /]$ ss -ltn
State                  Recv-Q                 Send-Q                                 Local Address:Port                                 Peer Address:Port                Process
LISTEN                 0                      511                                          0.0.0.0:80                                        0.0.0.0:*
LISTEN                 0                      511                                             [::]:80                                           [::]:*
```
🌞 Analyse trafic
[capture6](tp5_web.pcapng)                           
